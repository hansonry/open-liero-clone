settings = NewSettings()
local win_sdl_lib_path
if family == "windows" then
   
   if arch == "ia64" or arch == "amd64" then
      win_sdl_lib_path = "lib/windows-sdl-library/lib/msvc/x64/"
   else
      win_sdl_lib_path = "lib/windows-sdl-library/lib/msvc/x86/"
   end
   settings.cc.includes:Add("lib/windows-sdl-library/include")
   settings.link.libpath:Add(win_sdl_lib_path)
   settings.link.flags:Add("/SUBSYSTEM:CONSOLE")
   settings.link.libs:Add("SDL2main")
   settings.link.libs:Add("SDL2")
else
   settings.cc.flags:Add("`sdl2-config --cflags`")
   settings.link.flags:Add("`sdl2-config --libs`")
end

settings.link.libs:Add("SDL2_image")
sources = Collect("src/*.c")
objects = Compile(settings, sources)
exe = Link(settings, "open-liero-clone", objects)


if family == "windows" then
   local files = CopyToDirectory("./", Collect(win_sdl_lib_path .. "*.dll"))
   AddDependency(exe, files)
end
