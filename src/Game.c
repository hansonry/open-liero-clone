#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <SDL2/SDL_image.h>
#include "Game.h"
#include "TextureLoader.h"

#define PIXEL_FLAG_ACTIVE     0x01
#define PIXEL_FLAG_PHYS_SAND  0x02

struct pixel
{
   unsigned char r;
   unsigned char b;
   unsigned char g;
   unsigned char flags;
};

struct particle
{
   float x;
   float y;
   float vx;
   float vy;
};

enum cmd_walk
{
   ePCW_Stand,
   ePCW_Left,
   ePCW_Right
};

struct pawn
{
   float x;
   float y;
   float speed;
   struct particle particle; 
};

struct bullet
{
   struct particle particle;
   int active;
};

struct map
{
   int width;
   int height;
   struct pixel * data;
};

#define CAMERA_WIDTH   640
#define CAMERA_HEIGHT  480
struct camera
{
   int x;
   int y;
};

static struct BitmapFontRenderer * bfr;
static SDL_Texture * backgroundTexture;
static SDL_Texture * characterTexture;
static SDL_Texture * foregroundTexture;
static SDL_Texture * cursorTexture;
static SDL_Rect cursorRect;
static struct map map;
static struct camera camera;
static struct pawn player;
static struct bullet bullet;
static float physSandTimer_seconds;
static int pixelScanX, pixelScanY;

// Constants
static float gravity;
static float physSandTimeout_seconds;

static int Game_MapGetActive(int mapIndex)
{
   return (map.data[mapIndex].flags & PIXEL_FLAG_ACTIVE) == PIXEL_FLAG_ACTIVE;
}

static void Game_MapSetActive(int mapIndex, int active)
{
   if(active)
   {
      map.data[mapIndex].flags |= PIXEL_FLAG_ACTIVE;
   }
   else
   {
      map.data[mapIndex].flags &= ~PIXEL_FLAG_ACTIVE;
   }
}

static int Game_MapGetPhysSand(int mapIndex)
{
   return (map.data[mapIndex].flags & PIXEL_FLAG_PHYS_SAND) == PIXEL_FLAG_PHYS_SAND;
}

static void Game_MapSetPhysSand(int mapIndex, int sand)
{
   if(sand)
   {
      map.data[mapIndex].flags |= PIXEL_FLAG_PHYS_SAND;
   }
   else
   {
      map.data[mapIndex].flags &= ~PIXEL_FLAG_PHYS_SAND;
   }
}

#define Map_GetIndex(map, x, y) ((int)(y) * (map).width + (int)(x))

static int Map_IsInRange(struct map * map, int x, int y)
{
   return x >= 0 && x < map->width && y >= 0 && y <= map->height;
}

static void Game_LoadLevel(const char * imageFileName)
{
   SDL_Surface * surf;
   int x, y;
   int offset;
   Uint8 alpha;
   surf = IMG_Load(imageFileName);
   if(surf == NULL)
   {
      printf("Error: Failed to load Image %s\n", imageFileName);
   }
   else
   {
      map.width  = surf->w;
      map.height = surf->h;

      map.data = malloc(sizeof(struct pixel) * map.width * map.height);
      SDL_LockSurface(surf);
      for(y = 0; y < map.height; y++)
      {
         for(x = 0; x < map.width; x++)
         {
            Uint32 pixel; 
            offset = y * surf->pitch + x * surf->format->BytesPerPixel;
            pixel = *(Uint32*)&((unsigned char *)surf->pixels)[offset];
            offset = Map_GetIndex(map, x, y);
            SDL_GetRGBA(pixel, surf->format,
                        &map.data[offset].r,
                        &map.data[offset].g,
                        &map.data[offset].b,
                        &alpha);

            if(alpha > 0)
            {
               Game_MapSetActive(offset, 1);
            }
            else
            {
               Game_MapSetActive(offset, 0);
            }
            Game_MapSetPhysSand(offset, 0);
         }
      }
      
      SDL_UnlockSurface(surf);
      SDL_FreeSurface(surf);
   }
}

void Game_Init(SDL_Renderer * rend, struct BitmapFontRenderer * parentBFR)
{
   bfr = parentBFR;
   backgroundTexture = TextureLoader_Load(rend, "assets/Background.png", NULL, NULL);
   characterTexture  = TextureLoader_Load(rend, "assets/townfolk1_m.png", NULL, NULL);
   cursorTexture     = TextureLoader_Load(rend, "assets/Crosshair.png", &cursorRect.w, &cursorRect.h);
   Game_LoadLevel("assets/Level.png");
   foregroundTexture = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, CAMERA_WIDTH, CAMERA_HEIGHT);
   SDL_SetTextureBlendMode(foregroundTexture, SDL_BLENDMODE_BLEND);
   SDL_ShowCursor(SDL_DISABLE);

   gravity = 60;
   physSandTimer_seconds = physSandTimeout_seconds = 0.01;
   pixelScanX = 0;
   pixelScanY = 0;

   // Camera
   camera.x = 0;
   camera.y = 0;

   // Player Setup
   player.x = 100;
   player.y = 40;
   player.speed = 32;

   // Bullet Setup
   bullet.active = 0;
}

void Game_Destroy(void)
{
   free(map.data);
   SDL_DestroyTexture(backgroundTexture);
   SDL_DestroyTexture(foregroundTexture);
   SDL_DestroyTexture(characterTexture);
}

static void DrawCircleAt(int x, int y, int radius)
{
   int cx, cy, rx, ry;
   int impl;
   int index;
   for(cy = y - radius; cy < y + radius; cy++)
   {
      for(cx = x - radius; cx < x + radius; cx++)
      {
         if(Map_IsInRange(&map, cx, cy))
         {
            rx = cx - x;
            ry = cy - y;
            impl = rx * rx + ry * ry - radius * radius;
            if(impl < 0)
            {
               index = Map_GetIndex(map, cx, cy);
               Game_MapSetActive(index, 0);
            }
         }
      }
   }
}

void Game_Event(SDL_Event * event)
{
   if(event->type == SDL_MOUSEBUTTONDOWN)
   {
      if(event->button.button == SDL_BUTTON_MIDDLE)
      {
         DrawCircleAt(event->button.x, event->button.y, 5);
      }
      else if(event->button.button == SDL_BUTTON_LEFT)
      {
         int dx, dy;
         int cx, cy;
         double rad;
         cx = player.x;
         cy = player.y - 9;
         bullet.active = 1;
         bullet.particle.x = cx;
         bullet.particle.y = cy;
         dx = event->button.x - cx;
         dy = event->button.y - cy;
         rad = atan2(dy, dx);
         bullet.particle.vx = cos(rad) * 300;
         bullet.particle.vy = sin(rad) * 300;
      }
   }
}

static void Game_UpdateParticle(struct particle * particle, float seconds, float ax, float ay)
{
   particle->vx += ax * seconds;
   particle->vy += ay * seconds;
   particle->x += particle->vx * seconds;
   particle->y += particle->vy * seconds;
}

static int Game_Place(int x, int y, int * newy)
{
   int uy, dy;
   int up, dp;
   int found;
   int mapIndex;
   int ny;
   mapIndex = Map_GetIndex(map, x, y);
   up = dp = Game_MapGetActive(mapIndex);
   
   uy = y;
   dy = y;
   found = 0;
   while(!found)
   {
      if(uy < 0 || dy >= map.height)
      {
         break;
      }
      
      mapIndex = Map_GetIndex(map, x, uy);
      if(up == 1 && Game_MapGetActive(mapIndex) == 0)
      {
         //printf("Up Found\n");
         ny = uy + 1;
         found = 1;
         break;
      }
      up = Game_MapGetActive(mapIndex);
      uy--;
      mapIndex = Map_GetIndex(map, x, dy);
      if(dp == 0 && Game_MapGetActive(mapIndex) == 1)
      {
         //printf("Down Found\n");
         ny = dy;
         found = 1;
         break;
      }
      dp = Game_MapGetActive(mapIndex);
      dy++;
   }
   if(found && newy != NULL)
   {
      //printf("Found ny %i on x %i\n", ny, x);
      *newy = ny;
   }
   return found;

}

static void Game_UpdatePawn(struct pawn * pawn, float seconds)
{
   int mapIndex;
   const Uint8 * state;
   enum cmd_walk cmd_walk;
   int isOnGround;
   pawn->particle.x = pawn->x;
   pawn->particle.y = pawn->y;
   Game_UpdateParticle(&pawn->particle, seconds, 0, gravity);
   if(pawn->particle.x < 0)
   {
      pawn->particle.x = 0;
   }
   if(pawn->particle.x >= map.width)
   {
      pawn->particle.x = map.width - 1;
   }
   if(pawn->particle.y < 0)
   {
      pawn->particle.y = 0;
   }
   if(pawn->particle.y >= map.height)
   {
      pawn->particle.y = map.height - 1;
   }

   mapIndex = Map_GetIndex(map, pawn->particle.x, pawn->particle.y);

   if(Game_MapGetActive(mapIndex))
   {
      pawn->particle.vy = 0;
      pawn->particle.vx = 0;
      pawn->particle.y = pawn->y;
      isOnGround = 1;
   }
   else
   {
      isOnGround = 0;
   }
   pawn->x = pawn->particle.x;
   pawn->y = pawn->particle.y;

   // Handle Movement
   state = SDL_GetKeyboardState(NULL);
   if(state[SDL_SCANCODE_A])
   {
      cmd_walk = ePCW_Left;
   }
   else if(state[SDL_SCANCODE_D])
   {
      cmd_walk = ePCW_Right;
   }
   else
   {
      cmd_walk = ePCW_Stand;
   }
   if(isOnGround)
   {
      float newx;
      int newy;
      int dir;
      if(cmd_walk == ePCW_Left)
      {
         dir = -1;
      }
      else if(cmd_walk == ePCW_Right)
      {
         dir = 1;
      }
      else
      {
         dir = 0;
      }
      newx = pawn->x + pawn->speed * seconds * dir;
      if(Game_Place(newx, pawn->y, &newy))
      {
         if(newy > pawn->y)
         {
            // Below
            if(newy <= pawn->y + 5)
            {
               pawn->x = newx;
               pawn->y = newy;
            }
            else
            {
               //printf("Fall Hight: %f\n", newy - pawn->y);
               pawn->x = newx;
               pawn->particle.vx = pawn->speed * dir;
               //pawn->particle.vy = pawn->speed;
            }
         }
         else if(newy < pawn->y)
         {
            // Above
            if(newy >= pawn->y - 5)
            {
               pawn->x = newx;
               pawn->y = newy;
            }
         }
         else
         {
            pawn->x = newx;
            pawn->y = newy;
         }
      }

   }
}


static void Game_MapSwap(int mapIndex1, int mapIndex2)
{
   struct pixel temp;
   memcpy(&temp, &map.data[mapIndex1], sizeof(struct pixel));
   memcpy(&map.data[mapIndex1], &map.data[mapIndex2], sizeof(struct pixel));
   memcpy(&map.data[mapIndex2], &temp, sizeof(struct pixel));
}

static void Game_UpdateSandGrain(int x, int y, int mapIndex)
{
   int cx, cy, cMapIndex;
   cx = x;
   cy = y + 1;
   if(cy < map.height)
   {
      cMapIndex = Map_GetIndex(map, cx, cy);
      if(Game_MapGetActive(cMapIndex))
      {
         cx = x + 1;
         cMapIndex = Map_GetIndex(map, cx, cy);
         if(Game_MapGetActive(cMapIndex))
         {

            cx = x - 1;
            cMapIndex = Map_GetIndex(map, cx, cy);
            if(!Game_MapGetActive(cMapIndex))
            {
               Game_MapSwap(mapIndex, cMapIndex);
            }
         }
         else
         {
            Game_MapSwap(mapIndex, cMapIndex);
         }
         
      }
      else
      {
         Game_MapSwap(mapIndex, cMapIndex);
      }
   }
}

static int Game_MapCountNeghbors(int x, int y)
{
   int cx, cy;
   int xoffs[] = { 1, 1, 0, -1, -1  -1,  0,  1 };
   int yoffs[] = { 0, 1, 1,  1,  0, -1, -1, -1 };
   int i;
   int count;

   count = 0;
   for(i = 0; i < 8; i++)
   {
      int mapIndex;
      cx = x + xoffs[i];
      cy = y + yoffs[i];
      if(Map_IsInRange(&map, cx, cy))
      {
         mapIndex = Map_GetIndex(map, cx, cy);
         if(Game_MapGetActive(mapIndex))
         {
            count ++;
         }
      }
      else
      {
         count++;
      }

   }

   return count;

}

static void Game_UpdateSand(void)
{
   int x, y;
   int mapIndex;


   mapIndex = 0;
   for(y = map.height - 1; y >= 0; y--)
   {
      for(x = 0; x < map.width; x++)
      {
         mapIndex = Map_GetIndex(map, x, y);
         if(Game_MapGetActive(mapIndex) && 
            Game_MapGetPhysSand(mapIndex))
         {
            Game_UpdateSandGrain(x, y, mapIndex);
         }
      }
   }
}


static void Game_UpdatePixelScan(void)
{
   int mapIndex;
   int scancount;
   for(scancount = 0; scancount < 200; scancount ++)
   {
      mapIndex = Map_GetIndex(map, pixelScanX, pixelScanY);

      // Check for dust partical creation
      if(Game_MapGetActive(mapIndex) &&
         !Game_MapGetPhysSand(mapIndex) &&
         Game_MapCountNeghbors(pixelScanX, pixelScanY) <= 3)
      {
         Game_MapSetPhysSand(mapIndex, 1);
      }

      // Increment Scanner
      pixelScanX ++;
      if(pixelScanX >= map.width)
      {
         pixelScanX = 0;
         pixelScanY ++;
         if(pixelScanY >= map.height)
         {
            pixelScanY = 0;
         }
      }
   }
}

void Game_Update(float seconds)
{
   Game_UpdatePawn(&player, seconds);   
   if(bullet.active)
   {
      Game_UpdateParticle(&bullet.particle, seconds, 0, gravity);
      if(Map_IsInRange(&map, bullet.particle.x, bullet.particle.y))
      {
         int mapIndex;
         mapIndex = Map_GetIndex(map, bullet.particle.x, bullet.particle.y);
         if(Game_MapGetActive(mapIndex))
         {
            DrawCircleAt(bullet.particle.x, bullet.particle.y, 2);
            bullet.active = 0;
         }
      }
   }

   Game_UpdatePixelScan();

   physSandTimer_seconds -= seconds;
   if(physSandTimer_seconds <= 0)
   {
      physSandTimer_seconds += physSandTimeout_seconds;
      Game_UpdateSand();
   }

}

static void Game_RenderFPS(SDL_Renderer * rend, float fps)
{
   SDL_Rect rect;
   char buffer[256];
   sprintf(buffer, "FPS: %.1f", fps);
   SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
   rect.x = 10;
   rect.y = 10;
   rect.w = 50;
   rect.h = 24;
   BitmapFontRenderer_GetSize(bfr, buffer, &rect.w, &rect.h);
   SDL_RenderFillRect(rend, &rect);
   
   BitmapFontRenderer_Draw(rend, bfr, 10, 10, buffer);
}

static void Game_RenderBackgroundMask(void)
{
   unsigned char *pixels;
   int pitch;
   int y, x;
   int mapIndex;

   // Mask Texture
   SDL_LockTexture(foregroundTexture, NULL, (void**)&pixels, &pitch);
   
   mapIndex = 0;
   
   for(y = 0; y < CAMERA_HEIGHT; y++)
   {
      for(x = 0; x < CAMERA_WIDTH; x++)
      {
         if(!Game_MapGetActive(mapIndex))
         {
            pixels[0] = 0x00;
            pixels[1] = 0x00;
            pixels[2] = 0x00;
            pixels[3] = 0x00;
         }
         else
         {
            pixels[0] = 0xFF;
            pixels[1] = map.data[mapIndex].b;
            pixels[2] = map.data[mapIndex].g;
            pixels[3] = map.data[mapIndex].r;
         }
         pixels+=4;
         mapIndex++;
      }
   }

   SDL_UnlockTexture(foregroundTexture);
}

static void Game_DrawCharacter(SDL_Renderer * rend, int x, int y, int faceRight)
{
   SDL_Rect rSrc, rDest;
   rSrc.w = rDest.w = 16;
   rSrc.h = rDest.h = 18;

   rSrc.x = 0;
   if(faceRight)
   {
      rSrc.y = 18;
   }
   else
   {
      rSrc.y = 54;
   }

   rDest.x = x - 8;
   rDest.y = y - 18;
   SDL_RenderCopy(rend, characterTexture, &rSrc, &rDest);


}

void Game_Render(SDL_Renderer * rend, float fps)
{
   SDL_Rect rDest;
   int mx, my;
   SDL_GetMouseState(&mx, &my);
   mx /= 2;
   my /= 2;
   SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
   SDL_RenderClear(rend);
   


   SDL_RenderCopy(rend, backgroundTexture, NULL, NULL);

   Game_RenderBackgroundMask();

   rDest.x = 100;
   rDest.y = 100;
   rDest.w = 16;
   rDest.h = 16;
   SDL_RenderCopy(rend, foregroundTexture, NULL, NULL);
   //SDL_RenderCopy(rend, characterTexture, NULL, NULL);

   Game_DrawCharacter(rend, player.x, player.y, mx > player.x);

   if(bullet.active == 1)
   {
      SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
      SDL_RenderDrawPoint(rend, bullet.particle.x, bullet.particle.y);
   }

   // Render pixel scan
   SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
   SDL_RenderDrawPoint(rend, pixelScanX, pixelScanY);

   // Render Cursor
   SDL_GetMouseState(&cursorRect.x, &cursorRect.y);
   cursorRect.x = mx - 8;
   cursorRect.y = my - 8;
   SDL_RenderCopy(rend, cursorTexture, NULL, &cursorRect);

   Game_RenderFPS(rend, fps);


}


