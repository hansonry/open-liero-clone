#include <string.h>
#include "BitmapFontRenderer.h"

#define ASCII_OFFSET 32

static void BitmapFontRenderer_GetIndexCoords(int index,
                                              int widthInCharacters,
                                              int characterWidth,
                                              int characterHeight,
                                              int * x, int * y)
{
   int row;
   row = index / widthInCharacters;
   *x = (index - (row * widthInCharacters)) * characterWidth;
   *y = row * characterHeight;
   
}

void BitmapFontRenderer_Init(struct BitmapFontRenderer * bfr, 
                             SDL_Texture * texture, 
                             int textureWidth,
                             int textureHeight,
                             const char * characters,
                             char notFoundCharacter)
{
   size_t i;
   int widthInCharacters;
   int notFoundIndex;
   int newlineCount;
   int symbolRows;
   size_t numSyms;
   char * firstNewline;
   bfr->texture = texture;
   
   numSyms = strlen(characters);
   firstNewline = strchr(characters, '\n');
   if(firstNewline == NULL)
   {
      widthInCharacters = numSyms;
   }
   else
   {
      widthInCharacters = firstNewline - characters;
   }
   
   symbolRows = numSyms / widthInCharacters;
   if(numSyms % widthInCharacters != 0)
   {
      symbolRows++;
   }
   
   bfr->characterWidth  = textureWidth / widthInCharacters;
   bfr->characterHeight = textureHeight / symbolRows;
   
   
   // Find the notFoundCharacter
   notFoundIndex = 0;
   newlineCount = 0;
   for(i = 0; i < numSyms; i++)
   {
      if(characters[i] == notFoundCharacter)
      {
         notFoundIndex = i - newlineCount;
         break;
      }
      else if(characters[i] == '\n')
      {
         newlineCount++;
      }

   }
   BitmapFontRenderer_GetIndexCoords(notFoundIndex,
                                     widthInCharacters,
                                     bfr->characterWidth,
                                     bfr->characterHeight,
                                     &bfr->notFoundX,
                                     &bfr->notFoundY);

   for(i = 0; i < 95; i++)
   {
      bfr->xs[i] = bfr->notFoundX;
      bfr->ys[i] = bfr->notFoundY;
   }
   
   // count the number of newlines
   newlineCount = 0;
   for(i = 0; i < numSyms; i++)
   {
      if(characters[i] == '\n')
      {
         newlineCount++;
      }

   }
   
   
   for(i = numSyms - 1; i < numSyms; i--)
   {
      int sym = characters[i] - ASCII_OFFSET;
      if(sym >= 0 && 
         sym < 95)
      {
         BitmapFontRenderer_GetIndexCoords(i - newlineCount,
                                           widthInCharacters,
                                           bfr->characterWidth,
                                           bfr->characterHeight,
                                           &bfr->xs[sym],
                                           &bfr->ys[sym]);
      }
      else if(characters[i] == '\n')
      {
         newlineCount --;
      }
      
   }
   
}

void BitmapFontRenderer_Draw(SDL_Renderer * rend,
                             struct BitmapFontRenderer * bfr, 
                             int x, int y,
                             const char * text)
{
   SDL_Rect rSrc, rDest;
   int i, toDrawIndex;
   int sym;
      
   rSrc.w = bfr->characterWidth;
   rSrc.h = bfr->characterHeight;
   
   rDest.x = x;
   rDest.y = y;
   rDest.w = bfr->characterWidth;
   rDest.h = bfr->characterHeight;
   
   
   i = 0;
   while(text[i] != '\0')
   {
      sym = text[i] - ASCII_OFFSET;
      if(sym >= 0 && sym < 95)
      {
         rSrc.x = bfr->xs[sym];
         rSrc.y = bfr->ys[sym];
         
         SDL_RenderCopy(rend, bfr->texture, &rSrc, &rDest);
         
         rDest.x += bfr->characterWidth;
      }
      else if(text[i] == '\n')
      {
         rDest.x = x;
         rDest.y += bfr->characterHeight;
      }
      else
      {
         rSrc.x = bfr->notFoundX;
         rSrc.y = bfr->notFoundY;
         
         SDL_RenderCopy(rend, bfr->texture, &rSrc, &rDest);
         
         rDest.x += bfr->characterWidth;
      }
      i ++;
   }
}

void BitmapFontRenderer_GetSize(struct BitmapFontRenderer * bfr, 
                                const char * text, int * width, int * height)
{
   int lw, lh;
   int i;
   int sym;
   int cursorHeight;
   int cursorWidth;
   
   lw = 0;
   lh = 0;
   cursorHeight = 0;
   cursorWidth = 0;
   i = 0;
   
   while(text[i] != '\0')
   {
      sym = text[i] - ASCII_OFFSET;
      if(sym >= 0 && sym < 95)
      {
         
         cursorWidth += bfr->characterWidth;
         if(lw < cursorWidth)
         {
            lw = cursorWidth;
         }
         if(lh < cursorHeight + bfr->characterHeight)
         {
            lh = cursorHeight + bfr->characterHeight;
         }
      }
      else if(text[i] == '\n')
      {
         cursorHeight += bfr->characterHeight;
         cursorWidth = 0;
      }
      i++;
   }
   
   
   if(width != NULL)
   {
      *width = lw;
   }
   if(height != NULL)
   {
      *height = lh;
   }
}
