#include <stdio.h>
#include <SDL2/SDL_image.h>
#include "TextureLoader.h"

SDL_Texture * TextureLoader_Load(SDL_Renderer * rend, const char * filename, 
                                 int * width, int * height)
{
   SDL_Surface * surf;
   SDL_Texture * text;
   surf = IMG_Load(filename);
   if(surf == NULL)
   {
      printf("Error: Failed to load Image %s\n", filename);
      return NULL;
   }
   text = SDL_CreateTextureFromSurface(rend, surf);
   if(width != NULL)
   {
      *width = surf->w;
   }
   if(height != NULL)
   {
      *height = surf->h;
   }

   SDL_FreeSurface(surf);
   return text;
}
