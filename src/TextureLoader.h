#ifndef __TEXTURELOADER_H__
#define __TEXTURELOADER_H__
#include <SDL2/SDL.h>

SDL_Texture * TextureLoader_Load(SDL_Renderer * rend, const char * filename, 
                                 int * width, int * height);

#endif // __TEXTURELOADER_H__
