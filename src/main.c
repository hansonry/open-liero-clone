#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "BitmapFontRenderer.h"
#include "TextureLoader.h"
#include "Game.h"

struct SDLTimer
{
   Uint32 previousTime_ms;
};

void SDLTimer_Init(struct SDLTimer * timer)
{
   timer->previousTime_ms = SDL_GetTicks();
}

float SDLTimer_GetSecondsReset(struct SDLTimer * timer)
{
   Uint32 now_ms;
   Uint32 diff_ms;
   now_ms  = SDL_GetTicks();
   diff_ms = now_ms - timer->previousTime_ms;
   timer->previousTime_ms = now_ms;
   return diff_ms / 1000.0f;
}

float SDLTimer_DelayMaxFPS(struct SDLTimer * timer, int fps)
{
   Uint32 now_ms;
   Uint32 diff_ms;
   Uint32 fps_ms;
   float result;
   
   now_ms  = SDL_GetTicks();
   diff_ms = now_ms - timer->previousTime_ms;
   timer->previousTime_ms = now_ms;
   
   if(fps > 0)
   {
      fps_ms = 1000 / fps;
   }
   else
   {
      fps_ms = 0;
   }
   
   if(diff_ms < fps_ms)
   {
      SDL_Delay(fps_ms - diff_ms);
      result = fps;
   }
   else
   {
      result = 1000.0f / (float)diff_ms;
   }
   return result;
   
}


float filter(float new, float * base, int strength)
{
   *base = *base - *base / (float)strength + new;
   return *base / (float)strength;
}




int main(int argc, char * args[])
{
   SDL_Window * window;
   SDL_Renderer * rend;
   SDL_Event event;
   bool running;
   struct SDLTimer updateTimer, fpsTimer;

   SDL_Texture *font;
   int fontWidth, fontHeight;
   struct BitmapFontRenderer bfr;
   float fps = 0;
   float fpsFilter = 0;

   SDL_Init(SDL_INIT_EVERYTHING);
   IMG_Init(0);

   window = SDL_CreateWindow("Open Liero Clone", 
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             1280, 960, 
                             SDL_WINDOW_SHOWN);

   rend = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

   SDL_RenderSetLogicalSize(rend, 640, 480);

   font = TextureLoader_Load(rend, "assets/ProggyClean.png", &fontWidth, &fontHeight);
   BitmapFontRenderer_Init(&bfr, font,
                                 fontWidth, 
                                 fontHeight, 
                                 "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n"
                                 "abcdefghijklmnopqrstuvwxyz\n"
                                 "0123456789.?!/\\<>+-=|@#$%^\n"
                                 "&*(){}[]:;\"'~ ", 
                                 '?');
   Game_Init(rend, &bfr);
   SDLTimer_Init(&updateTimer);
   SDLTimer_Init(&fpsTimer);
   running = true;
   while(running)
   {
      // Input
      while(SDL_PollEvent(&event))
      {
         Game_Event(&event);
         if(event.type == SDL_QUIT)
         {
            running = false;
         }
         else if(event.type == SDL_KEYDOWN && 
                 event.key.keysym.sym == SDLK_ESCAPE)
         {
            running = false;
         }
      }

      // Update
      Game_Update(SDLTimer_GetSecondsReset(&updateTimer));
      
      // Render
      Game_Render(rend, fps);
      SDL_RenderPresent(rend);
      
      fps = filter(SDLTimer_DelayMaxFPS(&fpsTimer, 60), &fpsFilter, 5);
   }

   Game_Destroy();

   SDL_DestroyTexture(font);
   SDL_DestroyRenderer(rend);
   SDL_DestroyWindow(window);

   IMG_Quit();
   SDL_Quit();
   printf("End\n");
   return 0;
}

