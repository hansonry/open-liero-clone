#ifndef __GAME_H__
#define __GAME_H__
#include <SDL2/SDL.h>
#include "BitmapFontRenderer.h"

void Game_Init(SDL_Renderer * rend, struct BitmapFontRenderer * parentBFR);

void Game_Destroy(void);

void Game_Event(SDL_Event * event);

void Game_Update(float seconds);

void Game_Render(SDL_Renderer * rend, float fps);

#endif // __GAME_H__
