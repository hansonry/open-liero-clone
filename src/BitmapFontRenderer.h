#ifndef __BITMAPFONTRENDERER_H__
#define __BITMAPFONTRENDERER_H__
#include <SDL2/SDL.h>

struct BitmapFontRenderer
{
   SDL_Texture * texture;
   int characterWidth;
   int characterHeight;
   int xs[95];
   int ys[95];
   int notFoundX;
   int notFoundY;
};

// Note for multiple rows you need to put newlines into the characters string
void BitmapFontRenderer_Init(struct BitmapFontRenderer * bfr, 
                             SDL_Texture * texture, 
                             int textureWidth,
                             int textureHeight,
                             const char * characters,
                             char notFoundCharacter);

void BitmapFontRenderer_Draw(SDL_Renderer * rend,
                             struct BitmapFontRenderer * bfr, 
                             int x, int y,
                             const char * text);

void BitmapFontRenderer_GetSize(struct BitmapFontRenderer * bfr, 
                                const char * text, int * width, int * height);

                             
#endif // __BITMAPFONTRENDERER_H__
